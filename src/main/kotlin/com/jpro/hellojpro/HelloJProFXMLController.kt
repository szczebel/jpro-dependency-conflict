package com.jpro.hellojpro

import com.jpro.webapi.JProApplication
import com.jpro.webapi.WebAPI
import javafx.animation.Animation
import javafx.animation.FadeTransition
import javafx.animation.ParallelTransition
import javafx.animation.ScaleTransition
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.layout.StackPane
import javafx.util.Duration
import java.net.URL
import java.util.*

class HelloJProFXMLController : Initializable {
    @FXML
    var platformLabel: Label? = null

    @FXML
    var root: StackPane? = null

    @FXML
    var logo: Node? = null
    var jProApplication: JProApplication? = null
    var pt: ParallelTransition? = null

    override fun initialize(location: URL, resources: ResourceBundle?) {
        platformLabel!!.text = String.format("Platform: %s", if (WebAPI.isBrowser()) "Browser" else "Desktop")
    }

    protected fun initLogoAnimation(logo: Node?) {
        val st = ScaleTransition(Duration.millis(1000.0), logo)
        st.byX = -0.5
        st.byY = -0.5
        st.isAutoReverse = true
        st.cycleCount = Animation.INDEFINITE
        val ft = FadeTransition(Duration.millis(1000.0), logo)
        ft.toValue = 0.5
        ft.isAutoReverse = true
        ft.cycleCount = Animation.INDEFINITE
        logo!!.opacity = 1.0
        pt = ParallelTransition(st, ft)
        pt!!.play()
        if (WebAPI.isBrowser()) {
            jProApplication!!.webAPI.addInstanceCloseListener { pt!!.stop() }
        }
    }

    fun init(jProApplication: JProApplication?) {
        this.jProApplication = jProApplication
        initLogoAnimation(logo)
    }
}