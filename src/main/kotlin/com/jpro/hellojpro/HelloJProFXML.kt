package com.jpro.hellojpro

import com.jpro.webapi.JProApplication
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage
import java.io.IOException

class HelloJProFXML : JProApplication() {
    override fun start(stage: Stage) {
        //load user interface as FXML file
        val loader = FXMLLoader(javaClass.getResource("/com/jpro/hellojpro/fxml/HelloJPro.fxml"))
        var scene: Scene? = null
        try {
            val root = loader.load<Parent>()
            val controller = loader.getController<HelloJProFXMLController>()
            controller.init(this)

            //create JavaFX scene
            scene = Scene(root)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        stage.title = "Hello jpro!"
        stage.scene = scene

        //open JavaFX window
        stage.show()
    }
}

fun main(args: Array<String>) {
    JProApplication.launch(HelloJProFXML::class.java, *args)
}