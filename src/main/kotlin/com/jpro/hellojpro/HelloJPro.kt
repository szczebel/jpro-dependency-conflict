package com.jpro.hellojpro

import javafx.application.Application
import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.text.Font
import javafx.stage.Stage

class HelloJPro : Application() {
    override fun start(stage: Stage) {
        val label = Label("Hello JPro!")
        label.font = Font(50.0)
        label.alignment = Pos.CENTER
        stage.scene = Scene(label)
        stage.show()
    }
}

fun main(args: Array<String>) {
    Application.launch(HelloJPro::class.java, *args)
}