buildscript {
  repositories {
    gradlePluginPortal()
    maven(url = uri("https://sandec.jfrog.io/artifactory/repo"))
  }
  dependencies {
    classpath("com.sandec.jpro:jpro-plugin-gradle:2021.2.3")
  }
}
apply(plugin = "com.sandec.jpro")


plugins {
  application
  kotlin("jvm") version "1.6.10"
  id ("org.openjfx.javafxplugin") version "0.0.10"
  id("org.springframework.boot") version "2.6.3" apply false
  id("io.spring.dependency-management") version "1.0.11.RELEASE"
  kotlin("plugin.spring") version "1.6.10"
}

dependencyManagement {
  imports {
    mavenBom(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES)
  }
}

repositories {
  mavenCentral()
}

java {
  sourceCompatibility = JavaVersion.VERSION_11
  targetCompatibility = JavaVersion.VERSION_11
}

javafx {
  version = "17.0.1"
  modules("javafx.base", "javafx.graphics", "javafx.controls", "javafx.fxml", "javafx.media", "javafx.web")
}

application {
  mainClass.set("com.jpro.hellojpro.HelloJProFXML")
}

dependencies {
  implementation("org.jetbrains.kotlin:kotlin-reflect")

  implementation("org.springframework.boot:spring-boot-starter")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
  kotlinOptions {
    freeCompilerArgs = listOf("-Xjsr305=strict")
    jvmTarget = "17"
  }
}